export class MarketItem {
    ticker: string;
    lastClose: number;
    percentChange: number;

    constructor() {
        this.ticker = "";
        this.lastClose = 0;
        this.percentChange = 0;
    }
}
