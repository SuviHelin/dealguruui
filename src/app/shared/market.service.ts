import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MarketItem } from './market-item';
import { retry, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MarketService {

  apiURL = "https://dealguru-pricedata.s3.eu-west-1.amazonaws.com/priceData.json";

  constructor(private http: HttpClient) { }

  //Http Options
  httpOptions = {
    headers: new HttpHeaders ({
      'Content-Type':'application/json'
    })
  }

  //Http Client API getMarketItems() method
  getMarketItems(): Observable<MarketItem> {
    return this.http.get<MarketItem>(this.apiURL)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API getMarketItems() by ticker method
  getMarketItem(ticker:any): Observable<MarketItem> {
    return this.http.get<MarketItem>(this.apiURL + ticker)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API post() method (Create order)
  
  // HttpClient API put() method
  
  // HttpClient API delete() method
  deleteMarketItem(ticker:number){
    return this.http.delete<MarketItem>(this.apiURL + ticker, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
