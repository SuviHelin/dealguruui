export class Trade {
    id: number;
    stockTicker: string;
    name: string;
    side: string;
    profit: number;

    constructor() {
        this.id = 0;
        this.stockTicker = "";
        this.name = "";
        this.side = "";
        this.profit = 0;
    }
}
