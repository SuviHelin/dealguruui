import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from './order';
import { Trade } from './trade';
import { retry, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURL = environment.apiURL;

  constructor(private http: HttpClient) { }

  //Http Options
  httpOptions = {
    headers: new HttpHeaders ({
      'Content-Type':'application/json'
    })
  }

  //Order Methods
  //Http Client API getOrders() method
  getOrders(): Observable<Order> {
    return this.http.get<Order>(this.apiURL + "order")
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API getOrder() by id method
  getOrder(id:any): Observable<Order> {
    return this.http.get<Order>(this.apiURL + "order" + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API post() method (Create order)
  createOrder(order:Order): Observable<Order> {
    return this.http.post<Order>(this.apiURL + '/order', JSON.stringify(order), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API put() method
  updateOrder(id:number, order: Order): Observable<Order> {
    return this.http.put<Order>(this.apiURL + '/order', JSON.stringify(order), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method
  deleteOrder(id:number){
    return this.http.delete<Order>(this.apiURL + "order" + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Trade Methods
  //Http Client API getTrades() method
  getTrades(): Observable<Trade> {
    return this.http.get<Trade>(this.apiURL + "trade/")
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API getTrade() by id method
  getTrade(id:any): Observable<Trade> {
    return this.http.get<Trade>(this.apiURL + "trade/" + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API post() method (Create trade)
  createTrade(trade:Trade): Observable<Trade> {
    return this.http.post<Trade>(this.apiURL + '', JSON.stringify(trade), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API put() method
  updateTrade(id:number, trade: Trade): Observable<Trade> {
    return this.http.put<Trade>(this.apiURL + '', JSON.stringify(trade), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method
  deleteTrade(id:number){
    return this.http.delete<Trade>(this.apiURL + "trade/" + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
