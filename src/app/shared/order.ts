export class Order {
    id: number;
    name: string;
    stockTicker: string;
    price: number;
    volume: number;
    buyOrSell: string;
    timestamp: Date;
    status: string;

    constructor() {
        this.id = 0;
        this.name="Tesla, Inc. Common Stock";
        this.stockTicker = "TSLA";
        this.price = 0.00;
        this.volume = 0.00;
        this.buyOrSell = "";
        this.timestamp = new Date();
        this.status = "";
    }
}
