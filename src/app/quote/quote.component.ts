import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Order } from '../shared/order';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements OnInit {

  @Input() buydetails: Order = new Order();
  @Input() selldetails: Order = new Order();

  constructor(
    private restApi: RestApiService
  ) {
      this.selldetails.status = "INIT"
      this.buydetails.status = "INIT"
    }

  ngOnInit(): void {
  }

  latestBidPrice: number = 864.10;
  latestAskPrice: number = 865.80;

  selectTicker(event: any) {
    this.buydetails.stockTicker = event.target.value;
    this.buydetails.name = event.target.options[event.target.options.selectedIndex].text;

    this.selldetails.stockTicker = event.target.value;
    this.selldetails.name = event.target.options[event.target.options.selectedIndex].text;
  
    console.log(this.buydetails);
  }

  createBuy() {
    this.buydetails.buyOrSell = "BUY"
    if (this.buydetails.name == 'Apple Inc. Common Stock') {
      this.buydetails.stockTicker = "AAPL"
    }
    if (this.buydetails.name == 'Amazon.com, Inc. Common Stock') {
      this.buydetails.stockTicker = "AMZN"
    }
    if (this.buydetails.name == 'Intel Corporation Common Stock') {
      this.buydetails.stockTicker = "INTC"
    }
    if (this.buydetails.name == 'Netflix, Inc. Common Stock') {
      this.buydetails.stockTicker = "NFLX"
    }
    if (this.buydetails.name == 'Microsoft Corporation Common Stock') {
      this.buydetails.stockTicker = "MSFT"
    }
    if (this.buydetails.name == 'PepsiCo, Inc. Common Stock') {
      this.buydetails.stockTicker = "PEP"
    }
    if (this.buydetails.name == 'Atlassian Corporation Plc Class A Ordinary Shares') {
      this.buydetails.stockTicker = "TEAM"
    }
    if (this.buydetails.name == 'Tesla, Inc. Common Stock') {
      this.buydetails.stockTicker = "TSLA"
    }
    this.restApi.createOrder(this.buydetails).subscribe((data: any) =>{
      this.buydetails.price = 0;
      this.buydetails.volume = 0;
    })
  }

  createSell() {
    this.selldetails.buyOrSell = "SELL"
    if (this.selldetails.name == 'Apple Inc. Common Stock') {
      this.selldetails.stockTicker = "AAPL"
    }
    if (this.selldetails.name == 'Amazon.com, Inc. Common Stock') {
      this.selldetails.stockTicker = "AMZN"
    }
    if (this.selldetails.name == 'Intel Corporation Common Stock') {
      this.selldetails.stockTicker = "INTC"
    }
    if (this.selldetails.name == 'Netflix, Inc. Common Stock') {
      this.selldetails.stockTicker = "NFLX"
    }
    if (this.selldetails.name == 'Microsoft Corporation Common Stock') {
      this.selldetails.stockTicker = "MSFT"
    }
    if (this.selldetails.name == 'PepsiCo, Inc. Common Stock') {
      this.selldetails.stockTicker = "PEP"
    }
    if (this.selldetails.name == 'Atlassian Corporation Plc Class A Ordinary Shares') {
      this.selldetails.stockTicker = "TEAM"
    }
    if (this.selldetails.name == 'Tesla, Inc. Common Stock') {
      this.selldetails.stockTicker = "TSLA"
    }
    this.restApi.createOrder(this.selldetails).subscribe((data: any) =>{
      this.selldetails.price = 0;
      this.selldetails.volume = 0;
    })
  }

}
