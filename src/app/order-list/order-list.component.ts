import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { timer } from 'rxjs';

const source = timer(1000, 2000);

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  Orders: any = [];
  subscribe = source.subscribe(_ => this.loadOrders());

  constructor(public restApi: RestApiService) { }

  ngOnInit(): void {
    this.loadOrders()
  }

  loadOrders() {
    return this.restApi.getOrders().subscribe((data: {}) => {
      this.Orders = data;
    })
  }

  deleteOrder(id: any) {
    if (window.confirm('Are you sure you want to delete?')) {
      this.restApi.deleteOrder(id).subscribe(data => {
        this.loadOrders()
      })
    }
  }

}
