import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { timer } from 'rxjs';

const source = timer(2000, 2000);

@Component({
  selector: 'app-trade-list',
  templateUrl: './trade-list.component.html',
  styleUrls: ['./trade-list.component.css']
})
export class TradeListComponent implements OnInit {

  Trades: any = [];
  subscribe = source.subscribe(_ => this.loadTrades());
  
  constructor(public restApi: RestApiService) { }

  ngOnInit(): void {
    this.loadTrades()
  }

  loadTrades() {
    return this.restApi.getTrades().subscribe((data: {}) => {
        this.Trades = data;
        for (var i = 0; i < this.Trades.length; i++) {
          this.Trades[i].profit = this.getRandomNumberBetween(-0.14, 0.23)
        }
    })
  }

  deleteTrade(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteTrade(id).subscribe(data => {
        this.loadTrades()
      })
    }
  }
  
  getRandomNumberBetween(min:number,max:number){
    return Math.random()*(max-min+1)+min;
  }

}
