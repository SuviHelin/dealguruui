import { Component, OnInit } from '@angular/core';
import { MarketService } from "../shared/market.service";

@Component({
  selector: 'app-market-feed',
  templateUrl: './market-feed.component.html',
  styleUrls: ['./market-feed.component.css']
})
export class MarketFeedComponent implements OnInit {

  MarketItems: any = [];
  constructor(public marketApi: MarketService) { }

  ngOnInit(): void {
    this.loadMarketItems()
  }

  loadMarketItems() {
    return this.marketApi.getMarketItems().subscribe((data: {}) => {
        console.log(data)
        this.MarketItems = data;
    })
  }

  deleteMarketItem(ticker:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.marketApi.deleteMarketItem(ticker).subscribe(data => {
        this.loadMarketItems()
      })
    }
  } 
}
