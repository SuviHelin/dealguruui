import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketFeedComponent } from './market-feed.component';

describe('MarketFeedComponent', () => {
  let component: MarketFeedComponent;
  let fixture: ComponentFixture<MarketFeedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketFeedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
